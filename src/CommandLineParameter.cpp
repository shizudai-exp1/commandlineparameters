//============================================================================
// Name        : CommandLineParameter.cpp
// Author      : Yasuhiro Noguchi
// Version     : 0.0.1
// Copyright   : GPLv2
// Description : print command line parameter
//============================================================================

#include <iostream>
using namespace std;

int main(int argc, char** argv) {

	std::cout << "Number of arguments: " << argc << std::endl;
	std::cout << "program name: " << argv[0] << std::endl;

	// Rest parameters
	for ( int i=1; i<argc-1; i++ ) {
		std::cout << argv[i] << std::endl;
	}

	return 0;
}
